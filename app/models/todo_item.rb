class TodoItem < ApplicationRecord
  belongs_to :user
  
  validates :title, presence: true
  validates :avatar_url, 
    format: { with: %r{\A(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)\z}i},
    allow_blank: true
end
