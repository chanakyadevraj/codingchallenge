import React from "react";
import styles from "./AddTask.module.css";
const AddTask = ({ addTask }) => {
  let task = React.createRef();
  let imagePath = React.createRef();

  const addToList = () => {
    let url='';
    if(task.current.value!=='')
      {
        if(imagePath.current.value!=='')
        {
          url = imagePath.current.value.match(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)$/g);
          if(url === null)
          {
            imagePath.current.value = ""
            alert("Invalid image url.");
            return ;
          }
        }
        const taskData = {};
        taskData.title = task.current.value;
        taskData.avatar_url = imagePath.current.value;
        addTask(taskData);
      }
    else
      {
        alert("Task field is required");
      }
  }
  return (
    <div>
      <div className={styles.formContainer}>
        <div className={styles.formField}> Task</div>
        <input
          type="text"
          className={styles.inputText}
          ref={task}
        ></input>
        <div className={styles.formField}>Avatar</div>
        <input
          type="text"
          className={styles.inputText}
          ref={imagePath}
        ></input>
        <div className={styles.buttonContainer}>
          <button className={styles.addButton} onClick={() => addToList()}>
            Add
          </button>
        </div>
      </div>
    </div>
  );
};
export default AddTask;
