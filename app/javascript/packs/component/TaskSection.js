import React from 'react';
import styles from './TaskSection.module.css';
import avtar from './avtar.png';

import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
TimeAgo.addDefaultLocale(en);

import ReactTimeAgo from 'react-time-ago';

const TaskSection = ({ isDone, done, task ,time, taskId, url}) => 
{
  return (
    <div className={styles.taskList}>
      <div className={styles.taskSubheading}>
        <div className={styles.taskSubheading}>
        <img src={url || avtar} className={styles.userImage}/>
        </div>
        <div className={styles.taskListPara}>
          <div>{task}</div>
          </div>
        {done?
        <span className={styles.taskListSpan}>
          <ReactTimeAgo date={new Date(time)} locale="en-US"/>
        </span>
        :
        <div className={styles.checkBoxContainer}>
          <input
            type="checkbox"
            className={styles.checkBox}
            onChange={() => isDone(taskId)}
          />
        </div>
        }
      </div>
    </div>
  );
}
export default TaskSection;

