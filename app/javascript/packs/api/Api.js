import axios from 'axios'

export const getTasks = async () => {
    let result = await axios.get("/api/v1/todo_items");
    return result.data;
}

export const updateTask = async(index) => {
    let csrf = document.querySelector("meta[name='csrf-token']").getAttribute("content");
    let config = {
        method: 'post',
        url: '/api/v1/todo_items/'+index+'/mark_as_done',
        headers: { 
            'Content-Type': 'application/json',
            'X-CSRF-Token': csrf 
        }
    }
    let result = await axios(config);
    let data = await axios.get("/api/v1/todo_items");
    return data.data;
}

export const createTask = async(task) => {
    let csrf = document.querySelector("meta[name='csrf-token']").getAttribute("content");
    
    let taskData = {
        todo_item: task,
    }
    
    let config = {
        method: 'post',
        url: '/api/v1/todo_items',
        headers: { 
            'Content-Type': 'application/json',
            'X-CSRF-Token': csrf 
        },
        data: taskData
    }
    let result = await axios(config);
    if(result.data.status==="unprocessable_entity") alert("Invalid Avatar URL");
    let data = await axios.get("/api/v1/todo_items");
    return data.data;
}
