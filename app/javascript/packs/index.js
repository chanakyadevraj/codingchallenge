import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';


document.addEventListener('turbolinks:load', () => {
  const app = document.getElementById('root')
  app && ReactDOM.render(<App />, app);
})

serviceWorker.unregister();
