class Api::V1::TodoItemsController < ApplicationController
  before_action :authenticate_user!

  def index
    @todo_items = current_user.todo_items.order('updated_at desc').all
  end

  def create
    @todo_item = current_user.todo_items.build(todo_item_params)
    if authorized?
      respond_to do |format|
        format.json {
          if @todo_item.save
            render json: { status: :created }
          else
            render json: { status: :unprocessable_entity, error: @todo_item.errors.full_messages.join(', ') }
          end
        }
      end
    else
      handle_unauthorized
    end
  end
  
  def mark_as_done
    @todo_item = TodoItem.find_by(id: params[:todo_item_id])
    respond_to do |format|
      format.json {
      if @todo_item.update(completed_at: Time.zone.now)
        render json: { status: :ok, completed_at: @todo_item.reload&.completed_at&.to_i }
      else
        render json: @todo_item.errors.full_messages.to_sentence, status: :unprocessable_entity
      end
    }
    end
  end

  private

  def authorized?
    @todo_item.user = current_user
  end

  def handle_unauthorized
    unless authorized?
      respond_to do |format|
        format.json { render :unauthorized, status: 401 }
      end
    end
  end

  def todo_item_params
    params.require(:todo_item).permit(:title, :done, :avatar_url, :completed_at)
  end
end
