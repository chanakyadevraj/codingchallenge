# Coding Challenge Submission
This repo has 2 coding challenges in PDF format(One for Rails and one for Reactjs).   As a candidate, you must pick the challenge for which you are interviewing for.  
Follow the instructions and if they are unclear, please contact the hiring manager. 

Thank you very much for applying at DoubleGDP.

# Important!!
Do not submit a pull request to this repository.  You PR wil be rejected and your submission ignored.
To properly submit a coding challenge you must:

- fork this repository
- make the necessary changes
- push changes to your forked origin
- send address of your fork to the hiring manager.

We will review your fork online before and during your interview.



# Instructions to Setup Todo App

## Rails and Ruby versions
- Rails version - `6.0.3`
- Ruby version - `2.5.1`

## Setup

- RVM
`rvm install 2.5.1`

- Install bundler
`gem install bundler`

- Rails
`gem install rails -v 6.0.3`

- Bundle install

`bundle install`

- DB setup

`rake db:create`
`rake db:migrate`
`rake db:seed`

- Boot up rails server
`rails s` 

- Start webpacker
`./bin/webpack-dev-server`

## Access

- Home page
`http://localhost:3000`

# Run Specs

`rspec spec`


## Future enhancements

- Add [Material UI](https://material-ui.com/)
- Add filters to Completed and todo tasks
- Inline edit option would be awesome
- Delete / Archive Todo items
- Error handling can be made more roboust
- Modularise the current strcture
- Pagination

## Major upgarde proposals
- Assign Todo items to other users
- Shared Dashboard for multiple users
- Drag to set ordering 
