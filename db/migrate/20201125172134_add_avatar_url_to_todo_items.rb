class AddAvatarUrlToTodoItems < ActiveRecord::Migration[6.0]
  def change
    add_column :todo_items, :avatar_url, :string
  end
end
