class AddCompletedAtToTodoItems < ActiveRecord::Migration[6.0]
  def change
    add_column :todo_items, :completed_at, :datetime
    add_index :todo_items, :completed_at
  end
end
