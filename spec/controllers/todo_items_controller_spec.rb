# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::TodoItemsController do
  before do
    @user = create(:user)
  end

  before :each do
    sign_in @user
    request.env["HTTP_ACCEPT"] = 'application/json'
  end  


  describe 'GET #index' do
    let(:user) { create(:user) }
    it 'renders all todo_items list page' do
      get :index
      expect(response.status).to eq(200)
    end

    it 'assigns todo_items' do
      get :index
      expect(response.status).not_to be_nil
    end
  end

  describe 'POST #create' do
    let(:user) { create(:user) }

    it 'creates todo_item' do
      expect do
        post :create, params: { todo_item: { title: I18n.t('test.title'),
                                        avatar_url: I18n.t('test.url'),
                                        user_id: user.id } }
      end.to change(TodoItem, :count).by(1)
    end

    it 'returns valid message with json' do
      post :create, params: { todo_item: { title: I18n.t('test.title'),
                                      avatar_url: I18n.t('test.url'),
                                      user_id: user.id } }
      expect(response.body).to match({ status: :created }.to_json)
    end

    it 'checks presence of title with json' do
      post :create, params: { todo_item: { title: '',
                                      avatar_url: I18n.t('test.url'),
                                      user_id: user.id } }                         
      expect(response.body).to match({ status: 'unprocessable_entity', error: I18n.t('test.errors.title')}.to_json)
    end    
  end

  describe 'POST #mark_as_done' do
    let(:todo_item) { create(:todo_item) }

    it 'returns success true message' do
      post :mark_as_done, params: { todo_item_id: todo_item.id }, format: :json
      parsed_body = JSON.parse(response.body)
      expect(parsed_body['status']).to match('ok')
      expect(parsed_body['completed_at']).to be_within(5).of(Time.zone.now.to_i)
    end

    it 'changes the value of completed_at' do
      expect do
        post :mark_as_done, params: { todo_item_id: todo_item.id }, format: :json
      end.to change { todo_item.reload.completed_at }
    end
  end
end
