# frozen_string_literal: true

FactoryBot.define do
  factory :todo_item do
    title { 'This is a test title' }
    avatar_url { "#{Faker::Internet.url}.jpg" }
    user
  end
end
