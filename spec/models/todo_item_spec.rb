# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TodoItem, type: :model do
  describe 'schema' do
    it { is_expected.to have_db_column(:title).of_type(:string) }
    it { is_expected.to have_db_column(:avatar_url).of_type(:string) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  it 'has a valid factory' do
    expect(FactoryBot.build(:todo_item)).to be_valid
  end

  it 'is valid with a user id, avatar url and title' do
    expect(create(:todo_item)).to be_valid
  end

  it 'is invalid without a user' do
    todo_item = TodoItem.create(title: I18n.t('test.title'))
    todo_item.valid?
    expect(todo_item.errors[:user]).to include(I18n.t('test.errors.user'))
  end
end
